const shortId = require('shortid');
//const imageMin = require('imagemin');
//const imageMinWebp = require('imagemin-webp');
const path = require('path');

module.exports = {
    getById: async function (mediaId) {
        if(!mediaId)
            return null;
        let [media, ignored] = await sql.query("SELECT * FROM media WHERE id = ?", [mediaId]);
        if (media.length)
            return media[0];
        else
            return null;
    },
    getRefillsCustom: async function (table, filers, sort, from, pageSize, fullTextFields, fullTextValue) {
        let query = '';
        let whereClauses = [];
        let queryArguments = [];
        if (fullTextValue !== "" && fullTextValue !== null)
            whereClauses.push(fullTextFields.join('|') + " LIKE '%" + fullTextValue + "%'");
        for (const filter in filers) {
            if (filers.hasOwnProperty(filter) && filers[filter] && filers[filter] !== '') {
                whereClauses.push(filter + " = ?");
                queryArguments.push(filers[filter]);
            }
        }
        if (whereClauses.length > 0)
            query += " WHERE " + whereClauses.join(" AND ");

        let [count, ignored2] = await sql.query("SELECT COUNT(refill_media.id) AS count from media INNER JOIN refill_media ON media.id = refill_media.media_id where media.type = 'driver Refill' or media.type = 'rider Refill'");
        count = count[0].count;
        if (count === 0)
            return [];
        let [result, ignored] = await sql.query("select media.*,refill_media.id as rfId,refill_media.firstname,refill_media.lastname,refill_media.type,refill_media.media_id from media INNER JOIN refill_media ON media.id = refill_media.media_id where media.type = 'driver Refill' or media.type = 'rider Refill' "
          + " ORDER BY " + sort.property + " " + sort.direction + " LIMIT ? OFFSET ?",[parseInt(pageSize), parseInt(from)]);
        if (result.length > 0)
            result[0].count = count;
        return result;
    },
    getRefills: async function(){
        let result = await sql.query("select * from media where type = ? or type = ? ORDER BY id desc",['driver Refill','rider Refill']);
        return result;
    },
    removePicture: async function(mediaId) {
        try {
            let previousImage = await this.getById(mediaId);
            if (previousImage !== null && previousImage.address !== null) {
                previousImage = "/srv/" + previousImage.address;
                await fs.statAsync(previousImage);
                return fs.unlinkAsync(previousImage);
            }
        }
        catch (err) {
            console.log(err);
        }
    },
    updateDatabase: async function (mediaId,path) {
        let result = await sql.query("UPDATE media SET address = ?, path_type = 'relative' WHERE id = ?",[path,mediaId]);
        return result.affectedRows === 1;
    },
    doUpload: async function (buffer, mediaId) {
        const fileName = shortId.generate() + '.webp';
        let mediaType = (await this.getById(mediaId)).type;
        const relativePath = "img/" + mediaType + '/' + fileName;
        const fullPath = "/srv/" + relativePath;
        /*/let newBuffer = await imageMin.buffer(buffer, {
            use: [
                imageMinWebp({quality: 50})
            ]
        });*/
        if(!fs.isDir(fullPath))
            await fs.mkdirp("/srv/img/" + mediaType);
        let fd = await fs.openAsync(fullPath, 'a', 0o755);
        await fs.writeAsync(fd, buffer, 'binary');
        await fs.closeAsync(fd);
        await this.removePicture(mediaId);
        await this.updateDatabase(mediaId,relativePath);
        return (await mysql.getOneRow('media', {id: mediaId}));
    }

};
fs.isDir = function(dpath) {
    try {
        return fs.lstatSync(dpath).isDirectory();
    } catch(e) {
        return false;
    }
};
fs.mkdirp = async function(dirpath, mode) {
    dirpath = path.resolve(dirpath);

    if (typeof mode === 'undefined') {
        mode = parseInt('0777', 8) & (~process.umask());
    }

    try {
        if (!fs.statSync(dirpath).isDirectory()) {
            throw new Error(dirpath + ' exists and is not a directory');
        }
    } catch (err) {
        if (err.code === 'ENOENT') {
            fs.mkdirp(path.dirname(dirpath), mode);
            fs.mkdirSync(dirpath, mode);
        } else {
            throw err;
        }
    }
};
